<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Buku;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku_tabel =\App\Buku::all();
        $buku_tabel = DB::table('buku_tabel')->paginate(5);
        return view('buku.index', ['buku_tabel' => $buku_tabel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('buku.create');
    }

    public function delete($id)
    {
        DB::table('buku_tabel')->where('id', $id)->delete();
        return redirect('/Buku');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::table('buku_tabel')->insert([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
            'stok' => $request->stok
        ]);

        return redirect('/Buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $buku_tabel= DB::table('buku_tabel')->where('id',$id)->get();
        return view('buku.edit', ['buku_tabel' => $buku_tabel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        DB::table('buku_tabel')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'penerbit' => $request->penerbit,
            'stok' => $request->stok
        ]);

        return redirect('/Buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
