<?php


Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');

Route::get('/Buku', 'BukuController@index');

Route::get('/Buku/delete/{id}', 'BukuController@delete');

Route::get('/Buku/create', 'BukuController@create');
Route::post('/Buku/store', 'BukuController@store');

Route::get('/Buku/edit/{id}', 'Bukucontroller@edit');
Route::post('/Buku/update', 'BukuController@update');