@extends('layout.main')

@section('title', 'daftar buku')

  
@section('container')
    <div class="container"> 
    <div class="row">
    <div class="col-10"> 
    <h1 class="mt-3">Daftar Buku</h1>

    <a href="/Buku/create"> +Add Buku Baru </a>
  <br>

    <table class="table">
    <thead class="thead-dark">
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Buku</th>
        <th scope="col">Penerbit</th>
        <th scope="col">Stok</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    @foreach($buku_tabel as $buku)

    <tbody>
    <tr>
        <th scope="row">{{ $loop->iteration }}</th>
        <td>{{ $buku->nama }}</td>
        <td>{{ $buku->penerbit}}</td>
        <td>{{ $buku->stok}}</td>
        <td>
            <a href="/Buku/edit/{{ $buku->id }}" class="badge badge-success">edit</a>
            <a href="/Buku/delete/{{ $buku->id }}" class="badge badge-danger">delete</a>
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>

@endsection

    